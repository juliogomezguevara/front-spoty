import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SpotifySearchService {
  albumsFromDB: string = 'https://spoty-search.firebaseio.com/';
  albumsFromProvider: string = 'http://localhost:8080/search/';

  constructor(private http:HttpClient) { }


  searchInDB(key:string){  
    console.log('buscarmos en BD...');
    return this.http.get(this.albumsFromDB + key + '.json');
  }

  saveInDB(key:string, value:string){
    console.log('guardamos en BD...');
    this.http.put(this.albumsFromDB + key + '.json', value).subscribe();
  }

  searchInProvider(key:string){  
    console.log('buscamos en proveedor...');
    return this.http.get(this.albumsFromProvider + key);
  }

  

}
