import { Component } from '@angular/core';
import { SpotifySearchService } from './spotify-search.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  searchValue: string;
  resultSearch: string;

  constructor(private searchService:SpotifySearchService){
    
  }

  searchAlbum(){
    this.searchService.searchInDB(this.searchValue).subscribe(
      (dataInDB:string) => {
        if(dataInDB === null){
          this.searchService.searchInProvider(this.searchValue).subscribe(
            (dataInProvider) => {
              this.resultSearch = JSON.stringify(dataInProvider);
              this.searchService.saveInDB(this.searchValue, this.resultSearch);
            }
          )
        }else{
          this.resultSearch = JSON.stringify(dataInDB);
        }
      }
    );
  }
}
